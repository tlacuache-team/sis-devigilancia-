﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIVigilancia;

namespace SIVigilancia
{
    public partial class Form1 : Form
    {
        private WebCam objWebcam;
        int tamaño;
        public Form1()
        {
            InitializeComponent();
            tamaño = this.Width;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            iniciarCam();
            numericUpDown1.Value = 20;
            comboBox1.SelectedIndex = 2;
        }

        public void iniciarCam()
        {
            objWebcam = new WebCam(pictureBox1);
            objWebcam.inicializarWebCam_DetectarMovimiento();
        }
        public void reinciarCam()
        {
            objWebcam.stop();
            objWebcam = new WebCam(pictureBox1, Convert.ToInt16(numericUpDown1.Value), Convert.ToInt16(comboBox1.SelectedItem));
            objWebcam.inicializarWebCam_DetectarMovimiento();

        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                Video.imagenBN = false;
            }
            else
            {
                Video.imagenBN = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            iniciarCam();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            objWebcam.stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            objWebcam.configuracion();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            objWebcam.configuracion2();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            reinciarCam();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            Video.umbralRojo = Convert.ToByte(hScrollBarRojo.Value);
        }

        private void hScrollBar2_Scroll(object sender, ScrollEventArgs e)
        {
            Video.umbralVerde = Convert.ToByte(hScrollBarVerde.Value);
        }

        private void hScrollBar3_Scroll(object sender, ScrollEventArgs e)
        {
            Video.umbralAzul = Convert.ToByte(hScrollBarAzul.Value);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
           // button1.Width = this.Width + tamaño;
            //button2.Width = this.Width + tamaño;
            //button3.Width = this.Width + tamaño;
            //button4.Width = this.Width - tamaño;
            //button5.Width = this.Width - tamaño;
            //pictureBox1.Width = this.Width + tamaño;
            //pictureBox2.Width = this.Width + tamaño;

        }
    }
}
