﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using WebCam_Capture;

namespace SIVigilancia
{
    class WebCam
    {
        private PictureBox picture;
        private WebCamCapture objWebcam;
        private Video objSubstraccion;
        private Bitmap imagenAnterior;
        private Graphics gr;
        private PictureBox pictureBox1;
        private short v1;
        private short v2;

        public int resolucionImagen { get; set; }
        public int tiempoCaptura_Milisegundos { get; set; }

        public WebCam(PictureBox picture, int tiempoCaptura_Milisegundos = 30, int resolucionImagen = 300)
        {
            //Inicializamos propiedades
            this.tiempoCaptura_Milisegundos = tiempoCaptura_Milisegundos;
            this.resolucionImagen = resolucionImagen;
            this.picture = picture;
            this.imagenAnterior = new Bitmap(picture.Image);

            //Inicializamos objetos
            objWebcam = new WebCamCapture();
            objSubstraccion = new Video();
            gr = picture.CreateGraphics();
            this.objWebcam.TimeToCapture_milliseconds = this.tiempoCaptura_Milisegundos;
        }

        public WebCam(PictureBox pictureBox1, short v1, short v2)
        {
            this.pictureBox1 = pictureBox1;
            this.v1 = v1;
            this.v2 = v2;
        }

        public void stop()
        {
            objWebcam.Stop();
        }
        public void configuracion()
        {
            objWebcam.Config();
        }
        public void configuracion2()
        {
            objWebcam.Config2();
        }
        public void inicializarWebCam_DetectarMovimiento()
        {
            //Iniciamos cam y gestionamos evento
            objWebcam.Start(0);
            objWebcam.ImageCaptured += webcam_CapturarMovimiento;
        }

        private void webcam_CapturarMovimiento(Object source, WebcamEventArgs e)
        {
            //Calculamos la diferencia de imágenes
            picture.Image = objSubstraccion.substraer(new Bitmap(e.WebCamImage, this.resolucionImagen, this.resolucionImagen),
                new Bitmap(this.imagenAnterior, this.resolucionImagen, this.resolucionImagen));
            //Almacenamos la imagen actual como imagen anterior
            this.imagenAnterior = new Bitmap(e.WebCamImage);
            this.dibujarRecuadro();
        }

        private void dibujarRecuadro()
        {
            Pen lapiz = new Pen(Color.Red);
            Font fuente = new Font("Arial", 8);
            Brush brocha = Brushes.Red;
            string texto;

            picture.Refresh();

            //Calculamos coeficientes de reducción ancho/alto y calculamos el centro de la imagen
            float coefX = ((float)picture.Width) / resolucionImagen;
            float coefY = ((float)picture.Height) / resolucionImagen;
            PointF centro = new PointF(((coefX * Video.totalPixX) / Video.totalPixeles),
                ((coefY * Video.totalPixY) / Video.totalPixeles));

            //Delimitamos área de movimiento
            gr.DrawRectangle(lapiz, coefX * Video.minimoX, coefY * Video.minimoY, coefX * (Video.maximoX - Video.minimoX),
               coefY * (Video.maximoY - Video.minimoY));

            //Dibujamos cruz en el centro
            gr.DrawLine(lapiz, new PointF(centro.X - 4, centro.Y), new PointF(centro.X + 4, centro.Y));
            gr.DrawLine(lapiz, new PointF(centro.X, centro.Y - 4), new PointF(centro.X, centro.Y + 4));

            //Escribimos texto con coordenadas
            if (Video.minimoX != 1000 && Video.minimoY != 1000)
            {
                texto = Convert.ToString(Video.minimoX) + "," + Convert.ToString(Video.minimoY);
                gr.DrawString(texto, fuente, brocha, new PointF((coefX * Video.minimoX) - 15, (coefY * Video.minimoY) - 15));

                texto = Convert.ToString(Video.minimoX) + "," + Convert.ToString(Video.maximoY);
                gr.DrawString(texto, fuente, brocha, new PointF((coefX * Video.minimoX) - 15, (coefY * Video.maximoY)));

                texto = Convert.ToString(Video.maximoX) + "," + Convert.ToString(Video.minimoY);
                gr.DrawString(texto, fuente, brocha, new PointF((coefX * Video.maximoX) - 15, (coefY * Video.minimoY) - 15));

                texto = Convert.ToString(Video.maximoX) + "," + Convert.ToString(Video.maximoY);
                gr.DrawString(texto, fuente, brocha, new PointF((coefX * Video.maximoX) - 15, (coefY * Video.maximoY)));
            }




        }
    }
}
